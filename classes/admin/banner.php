<?php
	class banner_admin extends admin{
		
		public function index(){
			$db = Db::init();
			$col = $db->banner;
			$mcol = $col->findone();
			
			$error = array();
			$namafile = isset($mcol['logo']) ? trim($mcol['logo']) : '';
			
			if(!empty($_POST)) {
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('namafile', array('require'));
				$validator->setData(array('namafile' => $namafile));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					
					$data = array(
						'logo' => $namafileshafoto,
					);
					
					if(isset($mcol['_id']))
						$col->update(array('_id' => new MongoId($mcol['_id'])), array('$set' => $data));
					else {
						$data['time_created'] = time();
						$data['created_by'] = trim($_SESSION['userid']);
						$col->insert($data);
					}
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'chocho1')) {
								if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'chocho1/'.$namafile)) {
									if(! unlink(IMGPATH.'chocho1/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/banner/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'namafile' => $namafile,
				'judul' => ' Add / Edit Banner',
				'link' => '/admin/banner/index'
			);
			
			$this->render('banner', 'admin/banner/index.php', $var);
		}
	}
?>