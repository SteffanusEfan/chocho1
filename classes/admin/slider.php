<?php
	class slider_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_admingslider']) ? $_SESSION['search_admingslider'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_admingslider'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('title' => $regex)
				));			
			}
			
			$db = Db::init();
			$col = $db->sliders;
			
			$data = $col->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $col->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/slider/index?page=';
			$pg->calculate_pages($count, $limit, $page);	
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'page' => $page,
				'judul' => ' Sliders List',
				'search' => $search,
				'link' => '/admin/slider/index',
				'pagination' => $pg->Show()
			);
	
			$this->js[] = '/public/backend/controller/delete.js';
	
			$this->render("slider", "admin/slider/index.php", $var);
		}

		public function add() {
			$error = array();
			$title = '';
			$description = '';
			$namafile = '';
			
			if(!empty($_POST)) {
				$title = isset($_POST['title']) ? trim($_POST['title']) : '';
				$description = isset($_POST['description']) ? trim($_POST['description']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('title', array('require'));
				$validator->setData(array('title' => $title));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$data = array(
						'title' => $title,
						'description' => $description,
						'filename' => $namafileshafoto,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->sliders;
					$col->insert($data);
					
					if(strlen(trim($namafileshafoto)) > 0) {
						if(! is_dir(IMGPATH.'chocho1')) {
							if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
					}
					
					$this->redirect('/admin/slider/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
	
			$var = array(
				'error' => $error,
				'title' => $title,
				'description' => $description,
				'namafile' => $namafile,
				'judul' => ' Add New Slider',
				'link' => '/admin/slider/add'
			);
			
			$this->render('slider', 'admin/slider/add.php', $var);
		}
	
		public function delete() {
			$id = $_GET['id'];
			$page = $this->getPage();
			
			$db = Db::init();
			$col = $db->sliders;
			
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			if(isset($mcol['filename'])) {
				if(strlen(trim($mcol['filename'])) > 0) {
					if(file_exists(IMGPATH.'chocho1/'.$mcol['filename'])) {
						if(! unlink(IMGPATH.'chocho1/'.$mcol['filename'])){
							die('Failed to delete file...');
						}
					}
				}				
			}
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/slider/index?page='.$page);
		}
	}
?>