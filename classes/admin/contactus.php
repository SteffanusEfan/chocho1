<?php
	class contactus_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_adminpesan']) ? $_SESSION['search_adminpesan'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_adminpesan'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('email' => $regex)
				));			
			}
			$db = Db::init();
			
			$colpesan = $db->pesan;
			
			$data = $colpesan->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $colpesan->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/pesan/index?page=';
			$pg->calculate_pages($count, $limit, $page);
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'judul' => ' Pesan',
				'page' => $page,
				'search' => $search,
				'link' => '/admin/pesan/index',
				'pagination' => $pg->Show()
			);
			
			$this->js[] = '/public/backend/controller/delete.js';
			
			$this->render("pesan", "admin/pesan/index.php", $var);
		}

		public function delete() {
			$page = $this->getPage();
			$id = $_GET['id'];
			
			$db = Db::init();
			$col = $db->pesan;
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/pesan/index?page='.$page);
			exit;
		}
	}
?>