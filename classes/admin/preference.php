<?php
	class preference_admin extends admin{
		
		public function index(){
			$db = Db::init();
			$col = $db->preference;
			$mcol = $col->findone();
			
			$error = array();
			$name = isset($mcol['name']) ? trim($mcol['name']) : '';
			$address = isset($mcol['address']) ? trim($mcol['address']) : '';
			$email = isset($mcol['email']) ? trim($mcol['email']) : '';
			$phone = isset($mcol['phone']) ? trim($mcol['phone']) : '';
			$fax = isset($mcol['fax']) ? trim($mcol['fax']) : '';
			$narasi = isset($mcol['narasi']) ? trim($mcol['narasi']) : '';
			$namafile = isset($mcol['logo']) ? trim($mcol['logo']) : '';
			
			if(!empty($_POST)) {
				$name = isset($_POST['name']) ? trim($_POST['name']) : '';
				$address = isset($_POST['address']) ? trim($_POST['address']) : '';
				$email = isset($_POST['email']) ? trim($_POST['email']) : '';
				$phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
				$fax = isset($_POST['fax']) ? trim($_POST['fax']) : '';
				$narasi = isset($_POST['narasi']) ? trim($_POST['narasi']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('name', array('require'));
				$validator->setData(array('name' => $name));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					
					$data = array(
						'name' => $name,
						'address' => $address,
						'email' => $email,
						'phone' => $phone,
						'fax' => $fax,
						'narasi' => $narasi,
						'logo' => $namafileshafoto,
					);
					
					if(isset($mcol['_id']))
						$col->update(array('_id' => new MongoId($mcol['_id'])), array('$set' => $data));
					else {
						$data['time_created'] = time();
						$data['created_by'] = trim($_SESSION['userid']);
						$col->insert($data);
					}
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'chocho1')) {
								if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'chocho1/'.$namafile)) {
									if(! unlink(IMGPATH.'chocho1/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/preference/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'name' => $name,
				'address' => $address,
				'email' => $email,
				'phone' => $phone,
				'fax' => $fax,
				'narasi' => $narasi,
				'namafile' => $namafile,
				'judul' => ' Add / Edit Preference',
				'link' => '/admin/preference/index'
			);
			
			$this->render('preference', 'admin/preference/index.php', $var);	
		}
	}
?>