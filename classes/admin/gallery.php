<?php
	class gallery_admin extends admin{
		
		public function index() {
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_admingallery']) ? $_SESSION['search_admingallery'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_admingallery'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('title' => $regex)
				));			
			}
			
			$db = Db::init();
			$col = $db->gallery;
			
			$data = $col->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $col->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/gallery/index?page=';
			$pg->calculate_pages($count, $limit, $page);	
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'page' => $page,
				'judul' => ' Gallery List',
				'search' => $search,
				'link' => '/admin/gallery/index',
				'pagination' => $pg->Show()
			);
	
			$this->js[] = '/public/backend/controller/delete.js';
	
			$this->render("gallery", "admin/gallery/index.php", $var);
		}

		public function add() {
			$error = array();
			$caption = '';
			$namafile = '';
			$game = '';
			
			if(!empty($_POST)) {
				$caption = isset($_POST['caption']) ? trim($_POST['caption']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				$game = isset($_POST['game']) ? trim($_POST['game']) : '';
				
				$validator = new Validator();
				$validator->addRule('caption', array('require'));
				$validator->addRule('game', array('require'));
				$validator->setData(array('caption' => $caption,'game' => $game));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$data = array(
						'caption' => $caption,
						'filename' => $namafileshafoto,
						'game' => $game,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->gallery;
					$col->insert($data);
					
					if(strlen(trim($namafileshafoto)) > 0) {
						if(! is_dir(IMGPATH.'chocho1')) {
							if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
					}
					
					$this->redirect('/admin/gallery/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
	
			$var = array(
				'error' => $error,
				'caption' => $caption,
				'namafile' => $namafile,
				'judul' => ' Add New Gallery',
				'link' => '/admin/gallery/add'
			);
			
			$this->render('gallery', 'admin/gallery/add.php', $var);
		}
		
		public function edit() {
			$page = $this->getPage();
			$id = $_GET['id'];
			$db = Db::init();
			$col = $db->gallery;
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			
			$error = array();
			$caption = isset($mcol['caption']) ? trim($mcol['caption']) : '';
			$namafile = isset($mcol['filename']) ? trim($mcol['filename']) : '';
			$game = isset($mcol['game']) ? trim($mcol['game']) : '';
			
			if(!empty($_POST)) {
				$caption = isset($_POST['caption']) ? trim($_POST['caption']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				$game = isset($_POST['game']) ? trim($_POST['game']) : '';
				
				$validator = new Validator();
				$validator->addRule('caption', array('require'));
				$validator->addRule('game', array('require'));
				$validator->setData(array('caption' => $caption, 'game' => $game));
				
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$data = array(
						'caption' => $caption,
						'filename' => $namafileshafoto,
						'game' => $game
					);
					
					$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'chocho1')) {
								if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'chocho1/'.$namafile)) {
									if(! unlink(IMGPATH.'chocho1/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/gallery/index?page='.$page);
					exit;
				}
				else
					$error = $validator->getErrors();
			}
	
			$var = array(
				'error' => $error,
				'caption' => $caption,
				'namafile' => $namafile,
				'judul' => ' Edit Gallery',
				'game' => $game,
				'link' => '/admin/gallery/edit?id='.$id.'&page='.$page
			);
			
			$this->render('gallery', 'admin/gallery/add.php', $var);
		}

		public function delete() {
			$id = $_GET['id'];
			$page = $this->getPage();
			
			$db = Db::init();
			$col = $db->gallery;
			
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			if(isset($mcol['filename'])) {
				if(strlen(trim($mcol['filename'])) > 0) {
					if(file_exists(IMGPATH.'chocho1/'.$mcol['filename'])) {
						if(! unlink(IMGPATH.'chocho1/'.$mcol['filename'])){
							die('Failed to delete file...');
						}
					}
				}				
			}
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/gallery/index?page='.$page);
		}
	}
?>