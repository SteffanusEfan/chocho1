<?php 
	class games_admin extends admin{
		
		public function index(){
			$page = $this->getPage();
			$sort = $this->getSort();
			$tipesort = $this->getSortType();
			
			$limit = 10;
			$skip = (int)($limit * ($page-1));
			
			$search = isset($_SESSION['search_admingames']) ? $_SESSION['search_admingames'] : '';
			
			$arr = array();
			if(!empty($_POST)) {
				$search = trim($_POST['search']);			
				
				$_SESSION['search_admingames'] = $search;
			}
			
			$query = array();
			if(strlen(trim($search)) > 0) {
				$regex = new MongoRegex('/'.$search.'/i');
				$query = array(
					'$or' => array(
					array('namag' => $regex)
				));			
			}
			$db = Db::init();
			
			$colgames = $db->games;
			
			$data = $colgames->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
			$count = $colgames->count($query);
			
			$pg = new Pagination();
			$pg->pag_url = '/admin/games/index?page=';
			$pg->calculate_pages($count, $limit, $page);
			
			$var = array(
				'sort' => $sort,
				'tipesort' => $tipesort,
				'data' => $data,
				'judul' => ' Games',
				'page' => $page,
				'search' => $search,
				'link' => '/admin/games/index',
				'pagination' => $pg->Show()
			);
			
			$this->js[] = '/public/backend/controller/delete.js';
			
			$this->render("games", "admin/games/index.php", $var);
		}

		public function add(){
			$namafile= '';
			$namag = '';
			$highlight = '';
			$description = '';
			
			if(!empty($_POST)) {
				$namafile = isset($_POST['foto']) ? trim($_POST['foto']) : '';
				$namag = isset($_POST['namag']) ? trim($_POST['namag']) : '';
				$highlight = isset($_POST['highlight']) ? trim($_POST['highlight']) : '';
				$description = isset($_POST['description']) ? trim($_POST['description']) : '';
				
				$validator = new Validator();
				$validator->addRule('namag', array('require'));
				$validator->addRule('highlight', array('require'));			
				$validator->addRule('description', array('require'));
				
				$setdata = array(
					'namag' => $namag,
					'highlight' => $highlight,
					'description' => $description
				);						
				
				
				$validator->setData($setdata);
	
				if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }								
					
					$data = array(
						'namag' => $namag,
						'highlight' => $highlight,
						'description' => $description,
						'foto' => $namafileshafoto,
						'time_created' => time()
					);
					
					$db = Db::init();
					$col = $db->games;
					$col->insert($data);
					
					if(strlen(trim($namafileshafoto)) > 0) {
						if(! is_dir(IMGPATH.'chocho1')) {
							if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
							    die('Failed to create folders...');
							}
						}
						move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
					}
					
					$this->redirect('/admin/games/index');
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				//'error' => $error,
				'namag' => $namag,
				'highlight' => $narasi,
				'description' => $description,
				'foto' => $namafile,
				'link' => '/admin/games/add',
				'judul' => ' Add New Game'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';
			$this->js[] = '/public/backend/controller/autosize.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('games', 'admin/games/add.php', $var);
		}

		public function edit(){
			$page = $this->getPage();
			$id = $_GET['id'];
			$db = Db::init();
			$col = $db->games;
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			
			$error = array();
			$namag = isset($mcol['namag']) ? trim($mcol['namag']) : '';
			$highlight = isset($mcol['highlight']) ? trim($mcol['highlight']) : '';
			$description = isset($mcol['description']) ? trim($mcol['description']) : '';
			$namafile = isset($mcol['foto']) ? trim($mcol['foto']) : '';
			
			if(!empty($_POST)) {
				$namag = isset($_POST['namag']) ? trim($_POST['namag']) : '';
				$highlight = isset($_POST['highlight']) ? trim($_POST['highlight']) : '';
				$description = isset($_POST['description']) ? trim($_POST['description']) : '';
				$namafile = isset($_POST['namafile']) ? trim($_POST['namafile']) : '';
				
				$validator = new Validator();
				$validator->addRule('namag', array('require'));
				$validator->addRule('highlight', array('require'));
				$validator->addRule('description', array('require'));
							
				
				$setdata = array(
					'namag' => $namag,
					'highlight' => $highlight,
					'description' => $description
				);	
				
				$validator->setData($setdata);					
				
					if($validator->isValid()) {
					$foto = '';
		            $namafileshafoto = $namafile;
		            if(isset($_FILES['foto']['name']))
		            {
		                $foto = $_FILES['foto']['name'];
		                if(strlen(trim($foto)) > 0)
		                    $namafileshafoto = sha1(date('Y-m-d H:i:s', time()).$foto).'.'.helper::findexts($foto);
		            }
					
					$date = strtotime($released_date);
					
					$data = array(
						'namag' => $namag,
						'highlight' => $highlight,
						'description' => $description,
						'foto' => $namafileshafoto
					);
					
					
					$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
					
					if(isset($_FILES['foto']['name'])) {
						if($namafileshafoto !== $namafile) {
							if(! is_dir(IMGPATH.'chocho1')) {
								if (!mkdir(IMGPATH.'chocho1', 0777, true)) {
								    die('Failed to create folders...');
								}
							}
							
							if(strlen(trim($namafile)) > 0) {
								if(file_exists(IMGPATH.'chocho1/'.$namafile)) {
									if(! unlink(IMGPATH.'chocho1/'.$namafile)){
										die('Failed to delete file...');
									}
								}
							}
							
							move_uploaded_file($_FILES['foto']['tmp_name'], IMGPATH.'chocho1/'.$namafileshafoto);
						}
					}
					
					$this->redirect('/admin/games/index?page='.$page);
					exit;
				}
				else
					$error = $validator->getErrors();
			}
			
			$var = array(
				'error' => $error,
				'namag' => $namag,
				'highlight' => $highlight,
				'description' => $description,
				'namafile' => $namafile,
				'link' => '/admin/games/edit?id='.$id.'&page='.$page,
				'judul' => ' Edit Games Data'
			);
			
			$this->css[] = '/public/backend/plugins/select2/select2.css';
			$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
			$this->css[] = '/public/backend/plugins/tagsinput/tagsinput.css';
			
			$this->js[] = '/public/backend/plugins/select2/select2.min.js';
			$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';
			$this->js[] = '/public/backend/plugins/tagsinput/tagsinput.js';				
			$this->js[] = '/public/backend/controller/movie.js';
			
			$this->render('games', 'admin/games/add.php', $var);
		}

		public function delete() {
			$page = $this->getPage();
			$id = $_GET['id'];
			
			$db = Db::init();
			$col = $db->games;
			
			$mcol = $col->findone(array('_id' => new MongoId($id)));
			if(isset($mcol['foto'])) {
				if(strlen(trim($mcol['foto'])) > 0) {
					if(file_exists(IMGPATH.'chocho1/'.$mcol['foto'])) {
						if(! unlink(IMGPATH.'chocho1/'.$mcol['foto'])){
							die('Failed to delete file...');
						}
					}
				}				
			}
			
			$col->remove(array('_id' => new MongoId($id)));
			
			$this->redirect('/admin/games/index?page='.$page);
			exit;
		}
	}
?>