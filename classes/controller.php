<?php

class controller
{
	var $css;
	var $js;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();

		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
				
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
	    (include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
            
    
	protected function redirect($page)
	{
		header( 'Location: '.BASE_URL.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{
		$css[] = "/public/plugins/bootstrap/css/bootstrap.min.css";
		$css[] = "/public/css/theme.css";
		$css[] = "/public/css/custom.css";
		$css[] = "/css/helpers.min.css";
		$css[] = "/punlic/css/background.css";
		$font[] = "http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700";
		$css[] = "/public/plugins/font-awesome/css/font-awesome.min.css";
		$css[] = "/public/plugins/ionicons/css/ionicons.min.css";
		$css[] = "/public/plugins/animate/animate.min.css";
		$css[] = "/public/plugins/animate/animate.delay.css";
		$css[] = "/public/plugins/owl-carousel/owl.carousel.css";
 		
		$js[] = "/public/plugins/jquery/jquery-1.11.1.min.js";
		$js[] = "/public/plugins/bootstrap/js/bootstrap.min.js";
		$js[] = "/public/plugins/core.js";
		$js[] = "/public/plugins/owl-carousel/owl.carousel.min.js";
		
		
		
		
		$menuatas = $this->getView(DOCVIEW.'template/header_menu.php', $var);
		$var['menuatas'] = $menuatas;	
		
		$controller = $group;
		
		$content = $this->getView(DOCVIEW.$view, $var);
		
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);
		//if(strpos($_SERVER['HTTP_HOST'], 'dev') !== false)
			include(DOCVIEW."template/template.php");
		//elseif(($_SERVER['HTTP_HOST'] == 'kaningapictures.com') || ($_SERVER['HTTP_HOST'] == 'www.kaningapictures.com'))
		//else
			//include(DOCVIEW."template/maintenance/comingsoon.php");
	}
	
	protected function underrender($group,$view,$var)
	{
		include(DOCVIEW."under/index.php");
	}

}
?>