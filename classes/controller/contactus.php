<?php
	class contactus_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colb = $db->banner;
			$datab = $colb->findone();
			
			$var = array(
				'datab' => $datab
			);
			
			$this->js[] = 'http://maps.google.com/maps/api/js?sensor=true';
			$this->js[] = '/public/plugins/gmaps/prettify.js';
			$this->js[] = '/public/plugins/gmaps/gmaps.js';
			
			$this->render('contactus', '/contactus/index.php', $var);
		}
		
		public function add(){
			$db = Db::init();
			$q  = $db->pesan;
					
			if(!empty($_POST)){
				$dtemail = "";
				if(isset($_POST['email'])){
					$dtemail = trim($_POST['email']);
				}
				$dtsubject = "";
				if(isset($_POST['subject'])){
					$dtsubject = trim($_POST['subject']);
				}
				$dtmessage = "";
				if(isset($_POST['description'])){
					$dtmessage = trim($_POST['description']);
				}
				
				$db = Db::init();
				$q  = $db->message;
						
				$p  = array(
					'email' => $dtemail,
					'subject' => $dtsubject,
					'description' => $dtmessage,
					'time_created' => time(),
					'status'	=> "new"
				);
				$isi = $q->insert($p);
				
				$this->redirect("/");
				exit;
				
			}
			else {
				$this->redirect("/contact/index");
			}	
		}
	}
?>