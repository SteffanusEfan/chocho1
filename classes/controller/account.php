<?php

class account_controller
{
	public function login() {
		$error = array();
		$username = '';
		$pass = '';
		$action = '/login';
			
		if(!empty($_POST)) {
			$username = trim($_POST['username']);
			$pass = trim($_POST['pass']);
			
			$db = Db::init();
			$user = $db->user;
			$where = array('username' => $username);
			$col = $user->findOne($where);
			
			if(! isset($col['_id']))
				$error[] = array('error' => 'username doesnt exists');			
			else {				
				$oldpass = isset($col['password']) ? $col['password'] : ''; 
				
				if(helper::checkPassword($pass, $oldpass)) {
					$datal = array(
						'last_login' => time()
					);
					$wherel = array('_id' => new MongoId(trim($col['_id'])));
					$user->update($wherel,array('$set' => $datal));										
					
					$_SESSION['userid'] = trim($col['_id']);
					$_SESSION['name'] = $col['name'];
					$_SESSION['username'] = $username;
					
					echo '<html><meta http-equiv="refresh" content="0; url=/admin"></html>';
					exit;
				}
				
				$error[] = array('error' => 'wrong password');				
			}			
		}
		
		include(DOCVIEW."admin/template/login.php");
	}

	public function logout()
	{
		session_destroy ();
		
		header( 'Location: /' ) ;
		exit();
	}
		
	
	public function createuser() {
		//
		$salt = helper::getRandomString();
		$pass = helper::createPassword($salt, 'admin');
		
		$data = array(
			'name' => 'admin',
			'username' => 'admin',
			'password' => $pass,
			'salt' => $salt,
			'time_created' => time(),
			'last_login' => 0,
		);
		
		$db = Db::init();
		$users = $db->user;
		$user = $users->insert($data);
	}

	public function createuser2() {
		//
		$usr = new user();
		$salt = $usr->getRandomString();
		$pass = $usr->createPassword($salt, 'admin');
		echo $salt."<br/>";
		echo $pass;
	}
	
	public function changepassword() {
		$error = array();
		$judul = ' Change Password';
		$newpass = '';
		$repass = '';
		$action = '/changepassword';
		
		if(!empty($_POST)) {
			$newpass = trim($_POST['new']);
			$repass = trim($_POST['retype']);
			
			if(strlen(trim($newpass)) == 0)
				$error[] = array('error' => 'new password cannot empty.');
			elseif(strlen(trim($newpass)) < 3)
				$error[] = array('error' => 'new password too short, (3 characters min).');
			elseif(strlen(trim($repass)) == 0)
				$error[] = array('error' => 'retype password cannot empty.');
			elseif(!preg_match('/^'.$newpass.'$/', $repass))
				$error[] = array('error' => 'password not matches.');
			else {
				$salt = helper::getRandomString();
				$pass = helper::createPassword($salt, $newpass);
				
				$data = array(
					'password' => $pass,
					'salt' => $salt
				);
				
				$db = Db::init();
				$users = $db->user;
				$user = $users->update(array('_id' => new MongoId($_SESSION['userid'])), array('$set' => $data));
				
				echo '<html><meta http-equiv="refresh" content="0; url=/login"></html>';
				exit;
			}
		}
		
		include(DOCVIEW."admin/template/changepassword.php");
	}
}
