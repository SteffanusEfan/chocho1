<?php
	class gallery_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$col = $db->gallery;
			$colb = $db->banner;
			
			$colall = $col->find();
			$datab = $colb->findone();
			
			
			$var = array(
				'datall' => $colall,
				'datab' => $datab
			);
			$this->js[] = '/public/plugins/isotope/jquery.isotope.min.js';
			$this->js[] = '/public/plugins/ekko-lightbox/ekko-lightbox.min.js';
			$this->js[] = '/public/plugins/masonry/imagesloaded.pkgd.min.js';
			$this->js[] = '/public/plugins/masonry/masonry.pkgd.min.js';
			
			$this->render('gallery', '/gallery/index.php', $var);
		}
	}
?>