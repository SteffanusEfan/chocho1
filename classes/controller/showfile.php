<?php

class showfile_controller {
	
	public function index()
	{
		echo 'How Dee.....? '.date('d-m-Y', time());
	}
	
	public function show()
	{
		$namafile = $_GET['namafile'];
		$w = isset($_GET['w']) ? intval($_GET['w']) : 0;
		$h = isset($_GET['h']) ? intval($_GET['h']) : 0;
		$t = isset($_GET['t']) ? trim($_GET['t']) : '';
		$file = IMGPATH.'chocho1/'.$namafile;
		
		if (file_exists($file)) {
			$path_parts = pathinfo($file);
			$file_extension = '';
			if(isset($path_parts['extension']))
				$file_extension = $path_parts['extension'];
			$filename = $path_parts['filename'];
			
			if(($w > 0) && ($h > 0))
			{
				$pathname = CACHE.$filename;
				if($t == "")
				{
					$pathname .= '.'.$w.'x'.$h.'.'.$file_extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($file);
						$image->resizeToBestFit($w, $h);
						$image->save($pathname);
					}
					$file = $pathname;
				}
				else if($t == "h")
				{
					$pathname .= '.'.$w.'x'.$h.'-maxHeight.'.$file_extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($file);
						$image->resizeToHeight($h);
						$image->save($pathname);
					}
					$file = $pathname;
				}
				else if($t == "w")
				{
					$pathname .= '.'.$w.'x'.$h.'-maxWidth.'.$file_extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($file);
						$image->resizeToWidth($w);
						$image->save($pathname);
					}
					$file = $pathname;
				}
				else if($t == "e")
				{
					$pathname .= '.'.$w.'x'.$h.'-exact.'.$file_extension;
					if(!file_exists($pathname))
					{
						$image = new ImageResize($file);
						$image->crop($w, $h);
						$image->save($pathname);
					}
					$file = $pathname;
				}								
			}			
			//echo $file;die;
			switch( $file_extension ) {
			    case "gif": $ctype="image/gif"; break;
			    case "png": $ctype="image/png"; break;
			    case "jpeg": $ctype="image/jpeg"; break;
			    case "jpg": $ctype="image/jpg"; break;
			    default:
			}
			
			$last_modified_time = filemtime($file);
			$etag = md5_file($file);
			
	        if (!function_exists('getallheaders'))
	            $headers = $this->getAllHeaders();
	        else
	            $headers = getallheaders();
			
			if (array_key_exists("if-modified-since", $headers)) {
				http_response_code(304);
				exit;
			}
			else if (array_key_exists("if-none-match", $request->header)) {
				http_response_code(304);
				exit;
			}
			
			header("Last-Modified", gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
			header("Etag", $etag);
			header('Cache-Control', 'public');			
			header('Content-type: ' . $ctype);
			//header('content-disposition: inline; filename="'.$namafile.'";');
			ob_clean();
		    flush();
		    readfile($file);
		    exit;
		}
	}

	protected function getAllHeaders()
    {
       $headers = ''; 
       
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', ucwords(strtolower
                       (str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       
       return $headers; 
    }
}