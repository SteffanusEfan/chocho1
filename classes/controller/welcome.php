<?php
	class welcome_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colg = $db->games;
			$cols = $db->slider;
			
			$datag = $colg->find()->sort(array('time_created' => 1))->limit(6);
			$datas = $cols->find()->sort(array('time_created' => 1));
			
			$var = array(
				'datag' => $datag,
				'datas' => $datas
			);
			
			$this->render('home', '/welcome/index.php', $var);
		}
	}
?>