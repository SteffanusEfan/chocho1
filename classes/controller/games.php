<?php
	class games_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colg = $db->games;
			$colb = $db->banner;
			
			$datag = $colg->find()->sort(array('time_created' => 1));
			$datab = $colg->findone();
			
			$var = array(
				'datag' => $datag,
				'datab' => $datab
			);
			
			$this->render('games', '/games/index.php', $var);
		}
		
		public function detail(){
			$id = isset($_GET['id']) ? $_GET['id'] : '';
			
			$db = Db::init();
			$colgam = $db->games;
			
			$datagam = $colgam->findone(array('_id' => new MongoId(trim($id)))); 
			$datall = $colgam->find(array('_id' => new MongoId(array('$ne' => $id))));
			
			$var = array(
				'datagam' => $datagam,
				'datall' => $datall
			);
			
			$this->render('games', '/games/detail_game.php', $var);
		}
	}
?>