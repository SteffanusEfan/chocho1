$(document).ready(function() {
	$('body').delegate('.btn-add-language', 'click', function() {
		$.get('/util/addProduct', function(data) {
			$('#view-content').replaceWith(data);
			$("select.mws-select2").select2();
			$('textarea.description').cleditor({ width: '100%' });			
		});							
	});
	
	$("select.mws-select2").select2();
	$('textarea.description').cleditor({ width: '100%' });
});
