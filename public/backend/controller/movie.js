$(document).ready(function() {
	$('body').delegate('.use', 'change', function() {
		var val = $(this).val();
		
		$('#vrelease').html('');
		$('#cs_ondate').html('');
		$('#cs_onmonth').html('');
		if(val == 'release') {
			$.get('/util/getrelease', function(data) {
				$('#vrelease').html(data);
				$(".mws-datepicker").datepicker({
			        showOtherMonths: true, dateFormat: "dd-mm-yy"
			    });
			});
		}
		else if(val == 'coming_soon_on_date') {
			$.get('/util/getondate', function(data) {
				$('#cs_ondate').html(data);
				$(".mws-datepicker").datepicker({
			        showOtherMonths: true, dateFormat: "dd-mm-yy"
			    });
			});
		}
		else if(val == 'coming_soon_on_month') {
			$.get('/util/getonmonth', function(data) {
				$('#cs_onmonth').html(data);
				$("select.mws-select2").select2();
			});
		}
		
		return false;
	});
	$('body').delegate('click', '#release', function() {
		$.get('/util/getrelease', function(data) {
			$('#vrelease').html(data);
			$('#cs_ondate').html('');
			$('#cs_onmonth').html('');
			$(".mws-datepicker").datepicker({
		        showOtherMonths: true, dateFormat: "dd-mm-yy"
		    });
		});
		return false;
	});
	$('body').delegate('click', '#on_date', function() {
		$.get('/util/getondate', function(data) {
			$('#vrelease').html('');
			$('#cs_ondate').html(data);
			$('#cs_onmonth').html('');
			$(".mws-datepicker").datepicker({
		        showOtherMonths: true, dateFormat: "dd-mm-yy"
		    });
		});
		return false;
	});
	$('body').delegate('click', '#on_month', function() {
		$.get('/util/getonmonth', function(data) {
			$('#vrelease').html('');
			$('#cs_ondate').html('');
			$('#cs_onmonth').html(data);
			$("select.mws-select2").select2();
		});
		return false;
	});
	$(".mws-datepicker").datepicker({
        showOtherMonths: true, dateFormat: "dd-mm-yy"
    });
	$('input.tags').tagsInput();
	$('textarea.content').cleditor({ width: '100%' });
	$("select.mws-select2").select2();
});
