<div id="wrapper">
	<section class="hero" style="background-image: url(/showfile/show?filename='<?php echo $datab['filename']; ?>');">
		<div class="hero-bg-primary"></div>
		<div class="container">
			<div class="page-header">
				<div class="page-title">Contact Us</div>
			</div>
		</div>
	</section>

	<section class="padding-30">
		<div class="container text-center">
			<h2 class="font-size-22 font-weight-300">We would like to hear <span class="font-weight-500">about you</span> just send us a message!</h2>
		</div>
	</section>

	<section class="border-top-1 border-bottom-1 border-grey-400 section no-padding no-margin">
		<div id="map" class="height-300"></div>
	</section>
	
	<section class="border-top-1 border-bottom-1 border-grey-400 section no-padding no-margin">
		<div id="map" class="height-300"></div>
	</section>

	<section class="overflow-hidden">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="title">
						<h4><i class="fa fa-envelope"></i> Lets Get In Touch!</h4>
					</div>
					<form method="post" action="/contactus/add">
						<div class="form-group">
							<input id="email" type="text" class="form-control" placeholder="Email" required>
						</div>
						<div class="form-group">
							<input id="subject" type="text" class="form-control" placeholder="Subject" required>
						</div>
						<div class="form-group">
							<textarea id="description" class="form-control" rows="7" placeholder="Message"></textarea>
						</div>
						<div class="text-center margin-top-30">
							<button type="submit" class="btn btn-primary btn-lg btn-rounded btn-shadow">Send Message</button>
						</div>
					</form>
				</div>
				<div class="col-lg-5 height-300">
					<!--<img src="img/content/contact.jpg" class="image-right" alt="">-->
				</div>
			</div>
		</div>
	</section>
</div>
	
<script>
(function($) {
"use strict";
	var map;
	$(document).ready(function(){
		prettyPrint();
		var map = new GMaps({
			div: '#map',
			scrollwheel: false,
			lat: -6.876473,
			lng: 107.561476
		});
		var marker = map.addMarker({
			lat: -6.876,
			lng: -107.561476
		});
	});
})(jQuery);
</script>