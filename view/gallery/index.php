<div id="wrapper">	
	<section class="hero" style="background-image: url(/showfile/show?filename='<?php echo $datab['filename']; ?>');">
		<div class="hero-bg-primary"></div>
		<div class="container">
			<div class="page-header">
				<div class="page-title">Galleries</div>
			</div>
		</div>
	</section>

	<!--
	<section class="border-bottom-1 border-grey-300 padding-30">
		<div class="container text-center">
			<h2 class="font-size-22 font-weight-300">Choose from <span class="font-weight-500">12.000</span> available games</h2>
		</div>
	</section>-->
	
	
	<section class="bg-grey-50 padding-top-40 padding-xs-15">
		<div class="container margin-bottom-30">
			<ul id="filter" class="filter">
				<li><a href="#" data-filter="*" class="active">All</a></li>
				<?php
					foreach ($datall as $vd) {
						echo '<li><a href="#" data-filter="'.$vd['_id'].'">'.$vd['name'].'</a></li>';
					}
				?>
			</ul>
		</div>
		
		<?php
			foreach ($datall as $va) {
				echo '<div class="container gallery">
						<div class="row isotope masonry">
							<div id="'.$va['_id'].'" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 grid">
								<figure class="img-hover">
									<img src="/showfile/show?filename='.$va['foto'].'" alt="">
									<figcaption>
										<h2>'.$va['name'].'</h2>
										<a href="/showfile/show?filename='.$va['foto'].'" data-title="'.$va['name'].'" data-toggle="lightbox" data-gallery="multiimages"></a>
									</figcaption>			
								</figure>
							</div>
						</div>
					 </div>';
			}
		?>
	</section>
</div>

<script>
(function($) {
"use strict";
	var $container = $('.masonry');
	$($container).imagesLoaded( function(){
		$($container).masonry({
			itemSelector: '.grid', 
			columnWidth: '.grid'
		});
	});
		
	/*	Lightbox
	/*----------------------------------------------------*/
	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) { 
		event.preventDefault(); 
		$(this).ekkoLightbox();
	}); 
	
	/*	Isotope
	/*----------------------------------------------------*/
	$.fn.hideReveal = function( options ) {
	  options = $.extend({
		filter: '*',
		hiddenStyle: { opacity: 0.05 },
		visibleStyle: { opacity: 1 },
	  }, options );
	  this.each( function() {
		var $items = $(this).children();
		var $visible = $items.filter( options.filter );
		var $hidden = $items.not( options.filter );
		// reveal visible
		$visible.animate( options.visibleStyle );
		// hide hidden
		$hidden.animate( options.hiddenStyle );
	  });
	};

	var $container = $('.isotope');
	var $container_masonry = $('.masonry');
		  $container_masonry.imagesLoaded( function(){
			$container_masonry.isotope({
			filter: '*',
			animationOptions: {
			  easing: 'linear',
			  queue: false,
		   }
		});
	});
	
	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan50: function() {
		  var number = $(this).find('.number').text();
		  return parseInt( number, 10 ) > 50;
		},
		// show if name ends with -ium
		ium: function() {
		  var name = $(this).find('.name').text();
		  return name.match( /ium$/ );
			}
		};
  
		// bind filter button click
	$('#filter').on( 'click', 'a', function() {
		var filterValue = $( this ).attr('data-filter');
		// use filterFn if matches value
		filterValue = filterFns[ filterValue ] || filterValue;
		$container.hideReveal({ filter: filterValue });
		$container_masonry.hideReveal({ filter: filterValue });
		return false;
	  });

	// change is-checked class on buttons
	$('#filter').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );
		$buttonGroup.on( 'click', 'a', function() {
		  $buttonGroup.find('.active').removeClass('active');
		  $( this ).addClass('active');
		  return false;
		});
	});
})(jQuery);
</script>