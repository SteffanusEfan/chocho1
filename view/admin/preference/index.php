<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<!-- start logo 1 -->
    			<?php if(strlen(trim($namafile)) > 0) {?>
    			<div class="mws-form-row">
    				<label class="mws-form-label"></label>
    				<div class="mws-form-item">
    					<img width="500" src="/showfile/show?namafile=<?php echo $namafile; ?>" />
    				</div>
    			</div>
    			<?php }?>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Name <span class="required">*</span></label>
    				<div class="mws-form-item">
    					<?php $cls = isset($error['name']) ? ' error' : ''; ?>
    					<input type="text" name="name" value="<?php echo $name; ?>" class="small <?php echo $cls; ?>" />
    					<?php    						
	                	if(isset($error['name']))
						{
							echo '<div class="mws-error">';
							foreach($error['name'] as $message)
								echo $message;
							echo '</div>';
						}									                	
	                	?>
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Address</label>
    				<div class="mws-form-item clearfix">
    					<textarea name="address" rows="auto" cols="auto" class="small autosize"><?php echo $address; ?></textarea>    					
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Email</label>
    				<div class="mws-form-item">
    					<input type="text" name="email" value="<?php echo $email; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Phone</label>
    				<div class="mws-form-item">
    					<input type="text" name="phone" value="<?php echo $phone; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Fax</label>
    				<div class="mws-form-item">
    					<input type="text" name="fax" value="<?php echo $fax; ?>" class="small" />
    				</div>
    			</div>
    			<div class="mws-form-row">
    				<label class="mws-form-label">Narasi</label>
    				<div class="mws-form-item clearfix">
    					<textarea name="narasi" rows="auto" cols="auto" class="small autosize"><?php echo $narasi; ?></textarea>    					
    				</div>
    			</div>
			</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>