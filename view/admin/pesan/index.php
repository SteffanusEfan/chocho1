<?php
	$db = Db::init();
?>
<div class="mws-panel grid_8">
	<h2><?php echo $judul; ?></h2>
</div>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span class="pull-left"><i class="icon-table"></i> <?php echo $judul; ?></span>
    </div>
    <div class="mws-panel-toolbar">
    	<div class="btn-toolbar">
    		<div class="btn-group">
    			<!--<a href="/admin/gallery/add" class="btn btn-primary small" id="mws-themer-getcss"><i class="icon-plus"></i> Add New Data</a>-->
    		</div>
    	</div>
    </div>
    <div class="mws-panel-body no-padding">
    	<div class="dataTables_wrapper">
	    	<div class="dataTables_filter">
				<form method="post" action="<?php echo $link;?>">				
					<label><input type="text" placeholder="Search by tittle" name="search" value="<?php echo $search;?>"></label>
					<button type="submit" class="btn"><i class="icol-find"></i></button>
				</form>
			</div>
	        <table class="mws-table">
	            <thead>
	                <tr>
	                	<th>Email</th>
	                	<th>Subject</th>
	                    <th>Message</th>
	                    <th>Action</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php
	            foreach($data as $dat) {
	            	echo '<tr>';
						echo '<td width="80"></td>';
						echo '<td>'.ucwords($dat['email']).'</td>';
						echo '<td>'.ucwords($dat['subject']).'</td>';
						echo '<td>'.ucwords($dat['description']).'</td>';
						echo '<td width="120">';
						echo '<span class="btn-group">';
				        echo '<a href="#" link="/admin/pesan/delete?id='.$dat['_id'].'&page='.$page.'" rel="tooltip" data-placement="top" title="Delete Pesan" class="btn btn-small delete" data-controller="gallery" data-name="'.$dat['email'].'"><i class="icol-cancel"></i></a>';
				        echo '</span>';
				        echo '</td>';
					echo '</tr>';
	            }
	            ?>
	            </tbody>
	        </table>
	        <?php echo $pagination;?>
	    </div>
	</div>
</div>
<?php echo helper::showDialog(); ?>