<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><?php echo $judul;?></span>
    </div>
    <div class="mws-panel-body no-padding">
    	<form id="form-slider" class="mws-form" method="post" action="<?php echo $link; ?>" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<!-- start logo 1 -->
    			<?php if(strlen(trim($namafile)) > 0) {?>
    			<div class="mws-form-row">
    				<label class="mws-form-label"></label>
    				<div class="mws-form-item">
    					<img width="500" src="/showfile/show?namafile=<?php echo $namafile; ?>" />
    				</div>
    			</div>
    			<?php }?>
    		</div>
    		<div class="mws-button-row">
    			<input type="hidden" name="namafile" value="<?php echo $namafile; ?>" />
    			<input type="submit" value="Submit" class="btn btn-danger btn-submit">    			
    			<input type="reset" value="Reset" class="btn ">
    		</div>
    	</form>
    </div>    	
</div>