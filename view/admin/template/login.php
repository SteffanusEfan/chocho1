<!DOCTYPE html>
<html lang="en"><!--<![endif]-->
<Head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="/public/backend/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/public/backend/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="/public/backend/css/fonts/icomoon/style.css" media="screen">

<link rel="stylesheet" type="text/css" href="/public/backend/css/mws-style.css" media="screen">
<link rel="stylesheet" type="text/css" href="/public/backend/css/login.css" media="screen">

<link rel="stylesheet" type="text/css" href="/public/backend/css/mws-theme.css" media="screen">

<title>Kaningapictures - Admin Login</title>

</Head>

<Body>
    <div id="mws-login-wrapper">
    	<form class="mws-form" action="<?php echo $action;?>" method="post">
    	<?php
    	if(count($error) > 0) {
    	?>
    	<div class="mws-panel-body">
    		<div class="mws-form-message error">
    		<?php
    		foreach($error as $err)
				echo $err['error'];
    		?>
    		</div>
    	</div>
    	<?php }?>
        <div id="mws-login">
            <h1>Login</h1>
            <div class="mws-login-lock"><i class="icon-lock"></i></div>            
            <div id="mws-login-form">                
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="username" class="mws-login-username required" placeholder="Username" value="<?php echo $username;?>">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="pass" class="mws-login-password required" placeholder="password" value="<?php echo $pass;?>">
                        </div>
                    </div>
                    <!--div id="mws-login-remember" class="mws-form-row mws-inset">
                        <ul class="mws-form-list inline">
                            <li>
                                <input id="remember" type="checkbox"> 
                                <label for="remember">Remember me</label>
                            </li>
                        </ul>
                    </div-->
                    <div class="mws-form-row">
                        <input type="submit" value="Login" class="btn btn-success mws-login-button">
                    </div>                
            </div>
        </div>
        </form>
    </div>

    <!-- JavaScript Plugins -->
    <script src="/public/backend/js/libs/jquery-1.8.3.min.js"></script>
    <script src="/public/backend/js/libs/jquery.placeholder.min.js"></script>
    <script src="/public/backend/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="/public/backend/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="/public/backend/plugins/validate/jquery.validate-min.js"></script>
    <script src="/public/backend/js/core/mws.js"></script>

    <!-- Login Script -->
    <script src="/public/backend/js/core/login.js"></script>

</Body>
</html>