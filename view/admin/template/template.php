<!DOCTYPE html>
<html lang="en"><!--<![endif]-->
<Head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<?php
	if(isset($css))
	{
		foreach($css as $sc)
		{	
			echo '<link rel="stylesheet" href="'.$sc.'" media="screen" type="text/css" />';	
		}
	}
?>

<title>Chocho1 - Admin</title>

</Head>

<Body>

	<!-- Header -->
	<div id="mws-header" class="clearfix" >
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
    		<div id="mws-logo-wrap" >
            	<!--img height="50px" src="/public/images/logo.png" alt="Kakiatna" style="height: 50px; float:left; margin-left: 2px;"-->
			</div>
        </div>
        
        <!-- User Tools (notifications, logout, profile, change password) -->
        <div id="mws-user-tools" class="clearfix">
        
        	<!-- Notifications -->
        	<!--div id="mws-user-notif" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-exclamation-sign"></i></a>
                
                <!-- Unread notification count 
                <span class="mws-dropdown-notif"></span>
                -->
                <!-- Notifications dropdown ->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-notifications">
                        	<li class="read">
                            	<a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="read">
                            	<a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="#">
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="mws-dropdown-viewall">
	                        <a href="#">View All Notifications</a>
                        </div>
                    </div>
                </div>
            </div-->
            
            <!-- Messages -->
            <!--div id="mws-user-message" class="mws-dropdown-menu">
            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-envelope"></i></a>
                
                <!-- Unred messages count
                <span class="mws-dropdown-notif"></span>
                 -->
                <!-- Messages dropdown ->
                <div class="mws-dropdown-box">
                	<div class="mws-dropdown-content">
                        <ul class="mws-messages">
                        	<li class="read">
                            	<a href="#">
                                    <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="read">
                            	<a href="#">
                                    <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="#">
                                    <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        	<li class="unread">
                            	<a href="#">
                                    <span class="sender">John Doe</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet
                                    </span>
                                    <span class="time">
                                        January 21, 2012
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="mws-dropdown-viewall">
	                        <a href="#">View All Messages</a>
                        </div>
                    </div>
                </div>
            </div-->
            
            <!-- User Information and functions section -->
            <div id="mws-user-info" class="mws-inset">
            
            	<!-- User Photo -->
            	<div id="mws-user-photo">
            		<?php
            		if(isset($_SESSION['photo'])) {
            			if(strlen(trim($_SESSION['photo'])) > 0)
							echo '<img src="'.$_SESSION['photo'].'" alt="User Photo">';
            		}
            		?>                	
                </div>
                
                <!-- Username and Functions -->
                <div id="mws-user-functions">
                    <div id="mws-username">
                    	Hello,
                    	<?php
                    	echo isset($_SESSION['name']) ? ucwords($_SESSION['name']) : '';
                    	?>
                    </div>
                    <ul>
                    	<!--li><a href="/user/profile?id=<?php echo /*$_SESSION['userid'];*/ ''; ?>">Profile</a></li-->
                    	<li><a href="/changepassword">Change Password</a></li>
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        
        <!-- Sidebar Wrapper -->
        <div id="mws-sidebar">
        
            <!-- Hidden Nav Collapse Button -->
            <div id="mws-nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
            <!-- Searchbox -->
        	<div id="mws-searchbox" class="mws-inset" style="background: #FFF; text-align: center;">
            	<?php
            		$dprf = helper::getPreferences();
            		if(isset($dprf['logo'])) {
						if(strlen(trim($dprf['logo'])) > 0) {
							echo '<img id="main-logo" width="100%" src="/showfile/show?namafile='.$dprf['logo'].'" />';
						}
					}					
            	?>
            </div>
            
            <!-- Main Navigation -->
            <div id="mws-navigation">
            	<?php echo $leftmenu; ?>
            </div>         
        </div>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        	<!-- Inner Container Start -->
            <div class="container"> 
				<div style="clear: both"></div>
            	<?php echo $content; ?>
            </div>
            <!-- Inner Container End -->
                       
            <!-- Footer -->
            <div id="mws-footer">
            	Copyright &copy; Admin - Kaninga Pictures 2016.
            </div>
        </div>
        <!-- Main Container End -->
        
    </div>
	<?php
		if(isset($js))
		{
			foreach($js as $scr)
			{	
				echo '<script type="text/javascript" src="'.$scr.'"></script>';	
			}
		}
	?>
	<style>
	.cleditorMain{
		width:100% !important;
		height:300px !important;
	}
	</style>
</Body>
</html>