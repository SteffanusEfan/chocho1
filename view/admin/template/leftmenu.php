<ul>
	<?php	
	if($controller == '')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/"><i class="icon-home"></i> Dashboard</a>
	</li>
	
	<?php
	if($controller == 'slider')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/slider/index"><i class="icon-tools"></i> Slider</a>
	</li>
	
	<?php	
	if($controller == 'games')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/games/index"><i class="icon-th-large"></i> Games</a>
	</li>
	
	
	<?php
	if($controller == 'gallery')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/gallery/index"><i class="icon-tools"></i> Gallery</a>
	</li>
	
	<?php
	if($controller == 'banner')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/banner/index"><i class="icon-tools"></i> Banner</a>
	</li>
	
	<?php
	if($controller == 'preference')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/preference/index"><i class="icon-tools"></i> Preference</a>
	</li>
	
	<?php	
	if($controller == 'pesan')
		echo '<li class="active">';
	else
		echo '<li>';
	?>
		<a href="/admin/pesan/index"><i class="icon-th-large"></i> Message</a>
	</li>
		
</ul>
