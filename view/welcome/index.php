<div id="wrapper">	
	<div id="full-carousel" class="ken-burns carousel slide full-carousel carousel-fade" data-ride="carousel">
		<!--
		<ol class="carousel-indicators">
					<li data-target="#full-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#full-carousel" data-slide-to="1"></li>
					<li data-target="#full-carousel" data-slide-to="2"></li>
				</ol>-->
		
		<div class="carousel-inner">
			<!-- slider start -->
			<?php
				foreach ($datas as $value) {
					echo '<div class="item active inactiveUntilOnLoad">
							<img src="showfile/show?namafile='.$value['filename'].'" alt="">
							<div class="container">
								<div class="carousel-caption">
									<h1 data-animation="animated animate1 bounceInDown">'.$value['title'].'</h1>
									<p data-animation="animated animate7 fadeInUp">'.$value['description'].'</p>
								</div>		
							</div>
						  </div>';
				}
			?>
			<!--
			<a class="left carousel-control" href="#full-carousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						</a>
						<a class="right carousel-control" href="#full-carousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						</a>-->
			
			<!-- slider end -->
		</div>
	</div>
	
	<section class="bg-grey-50 border-bottom-1 border-grey-200 relative">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="title outline">
						<h4><i class="fa fa-heart"></i> Games</h4>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu leo lobortis, aliquam dui.</p>-->
					</div>
				</div>
			</div>
			
			<?php 
				foreach ($datag as $vg) {
					echo '<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="card card-hover">
									<div class="card-img">
										<img src="/showfile/show?namafile='.$vg['foto'].'" alt="">
										<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>15</span></a></div>
									</div>
									<div class="caption">
										<h3 class="card-title"><a href="games-single.html">'.$vg['namag'].'</a></h3>
										<ul>
											<li>'.date('dd-mm-YY', $vg['time_created']).'</li>
										</ul>
										<p>'.$vg['highlight'].'</p>
									</div>
								</div>
							</div>
						</div>';
				}
			?>
			<!--<div class="text-center"><a href="games.html" class="btn btn-primary btn-lg btn-shadow btn-rounded btn-icon-right margin-top-10 margin-bottom-20">Show More <i class="fa fa-angle-right"></i></a></div>-->
		</div>
	</section>
			
	<!--
	<section>	
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="title outline">
							<h4><i class="fa fa-star"></i> Recent Reviews</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu leo lobortis, aliquam dui.</p>
						</div>
					</div>
				</div>
				<div class="row slider">
					<div class="owl-carousel">
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/1.jpg" alt="">
								<span class="label label-success">7.2</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Uncharted 4</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/2.jpg" alt="">
								<span class="label label-warning">5.4</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Hitman: Absolution</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/3.jpg" alt="">
								<span class="label label-danger">2.1</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Last of Us 2</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/4.jpg" alt="">
								<span class="label label-success">6.9</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Bioshock: Infinite</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/5.jpg" alt="">
								<span class="label label-success">7.2</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Grand Theft Auto 5</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/6.jpg" alt="">
								<span class="label label-warning">5.4</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">DayZ</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
						
						<div class="card card-list">
							<div class="card-img">
								<img src="img/review/7.jpg" alt="">
								<span class="label label-danger">2.1</span>
							</div>
							<div class="caption">
								<h4 class="card-title"><a href="reviews-single.html">Liberty City</a></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
					<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		</section>-->
	
		
	<!-- video
	<div class="background-image margin-top-40" style="background-image: url(http://img.youtube.com/vi/IsDX_LiJT7E/maxresdefault.jpg);">
			<span class="background-overlay"></span>
			<div class="container">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IsDX_LiJT7E?rel=0&amp;showinfo=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>-->
	
		
	<!-- belum perlu
	<section class="bg-success subtitle-lg">
			<div class="container">
				<h2>Create your own epic gaming site with gameforest template</h2>
				<a href="http://themeforest.net/item/gameforest-responsive-gaming-html-theme/5007730" target="_blank" class="btn btn-white btn-outline">Purchase Now<i class="fa fa-shopping-cart margin-left-10"></i></a>
			</div>
		</section>-->
	
</div>