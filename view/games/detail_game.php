<div id="wrapper">	
	<section class="padding-top-50 padding-bottom-50 padding-top-sm-30">
		<div class="container">
			<div class="row sidebar">
				<div class="col-md-9 leftside">
					<div class="post post-single">
						<div class="post-header post-author">
							<a href="#" class="author" data-toggle="tooltip" title="YAKUZI"><img src="/showfile/show?filename='<?php echo $datagam['foto'] ?>'" alt="" /></a>
							<div class="post-title">
								<h2><a href="#"><?php echo $datagam['name']; ?></a></h2>
								<ul class="post-meta">
									<!--<li><a href="#"><i class="fa fa-user"></i> YAKUZI</a></li>-->
									<li><i class="fa fa-calendar-o"></i> <?php echo date('mm-dd-YY',$datag['time_created']); ?></li>
									<!--<li><a href="#"><i class="fa fa-comments"></i> 0 <span class="hidden-xs">Comments</span></a></li>-->
								</ul>
							</div>
						</div>
						
						<!--
						<div class="post-thumbnail">
							<a href="#"><img src="img/blog/lg/post.jpg" alt=""></a>
							<div class="post-caption">Example of post thumbnail caption</div>
						</div>-->
						
						
						<?php echo '<p>'.$datagam['description'].'</p>'; ?>
							
						<!--
						<div class="row margin-top-40">
							<div class="col-md-8">
								<div class="tags">
									<a href="#">Playstation 4</a>
									<a href="#">Xbox One</a>
									<a href="#">Mirrors Edge</a>
									<a href="#">Cod Black Ops 3</a>
									<a href="#">Battlefront 3</a>
								</div>
							</div>
							<div class="col-md-4 hidden-xs hidden-sm">
								<ul class="share">	
									<li><a href="#" class="btn btn-sm btn-social-icon btn-facebook" data-toggle="tooltip" title="Share on facebook"><i class="fa fa-facebook"></i></a><span>312</span></li>
									<li><a href="#" class="btn btn-sm btn-social-icon btn-twitter" data-toggle="tooltip" title="Share on twitter"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="btn btn-sm btn-social-icon btn-google-plus" data-toggle="tooltip" title="Share on google"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</div>	-->
						
					</div>
						
					<!--
					<div class="comments">
						<h4 class="page-header"><i class="fa fa-comment-o"></i> Comments (5)</h4>
						<a href="#" class="btn btn-block btn-primary text-left margin-bottom-30"><i class="fa fa-spinner fa-pulse margin-right-10"></i> Load more 4 comments</a>
						<div class="media">
							<a class="media-left" href="#">
								<img src="img/user/avatar2.jpg" alt="" />
							</a>
							<div class="media-body">
								<div class="media-content">
									<a href="#" class="media-heading">Nathan Drake</a>
									<a href="#" class="btn btn-sm btn-primary pull-right">reply</a>
									<span class="date">April 15, 2016 at 13:00 am</span>
									<p>Integer hendrerit, quam sit amet venenatis consequat, nisi nisi faucibus sapien, id scelerisque nisi magna in justo. Proin ac ex cursus, congue orci eu, euismod lorem.</p>
								</div>
							</div>
						</div>
								
						<div class="media">
							<a class="media-left" href="#">
								<img src="img/user/avatar3.jpg" alt="" />
							</a>
							<div class="media-body">
								<div class="media-content">
									<a href="#" class="media-heading">Tomb Raider</a>
									<a href="#" class="btn btn-sm btn-primary pull-right">reply</a>
									<span class="date">April 15, 2016 at 13:00 am</span>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam aliquet lacinia arcu. Suspendisse laoreet neque nisi, et ultrices neque viverra ut. Praesent at dapibus velit, id pharetra arcu.</p>
								</div>
										
								<div class="media">
									<a class="media-left" href="#">
										<img src="img/user/avatar.jpg" alt="" />
									</a>
									<div class="media-body">
									<div class="media-content">
										<a href="#" class="media-heading">YAKUZI</a>
										<a href="#" class="btn btn-sm btn-primary pull-right">reply</a>
										<span class="date">April 15, 2016 at 13:00 am</span>
										<p>Duis ultricies commodo accumsan. Donec augue leo, feugiat lobortis ex ut, convallis blandit arcu.</p></div>
									</div>
								</div>
							</div>
						</div>
							
						<div class="media">
							<a class="media-left" href="#">
								<img src="img/user/avatar4.jpg" alt="" />
							</a>
							<div class="media-body">
								<div class="media-content">
									<a href="#" class="media-heading">Trevor Philips</a>
									<a href="#" class="btn btn-sm btn-primary pull-right">reply</a>
									<span class="date">April 15, 2016 at 13:00 am</span>
									<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </p>
								</div>
							</div>
						</div>
						
						<div class="comment-form">
							<h4 class="page-header">Leave a comment</h4>
							<form>
								<div class="form-group">
									<textarea class="form-control" rows="6" placeholder="Your Comment"></textarea>
								</div>
								<button type="button" class="btn btn-primary btn-rounded btn-shadow pull-right">Submit Comment</button>
							</form>
						</div>
					</div>-->
					
				</div>
				
				
				<div class="col-md-3 rightside">
					<div class="widget margin-bottom-35">
						<div class="btn-inline">
							<input type="text" class="form-control padding-right-40"  placeholder="Search...">
							<button type="submit" class="btn btn-link color-grey-700 padding-top-10"><i class="fa fa-search"></i></button>
						</div>
					</div>

					<div class="widget widget-list">
						<div class="title">Recent Posts</div>
						<ul>
							<?php 
								foreach ($datall as $va) {
									echo '<li>
											<a href="/games/detail_game?id='.$va['_id'].'" class="thumb"><img src="/showfile/show?filename= '.$vg['foto'].'" alt=""></a>
											<div class="widget-list-meta">
												<h4 class="widget-list-title"><a href="/games/detail_game?id='.$va['_id'].'">'.$vg['name'].'</a></h4>
												<p><i class="fa fa-clock-o"></i>'.date('mm-dd-YY', $vg['time_created']).'</p>
											</div>
										  </li>';
								}
							?>
						</ul>
					</div>
					
					<!--
					<div class="widget">
						<a href="#"><img src="img/place.jpg" alt=""></a>
					</div>-->
					
					<!--
					<div class="widget widget-list">
						<div class="title">Recent Comments</div>
						<ul>
							<li>
								<a href="#" class="thumb"><img src="img/blog/xs/1.jpg" alt=""></a>
								<div class="widget-list-meta">
									<h4 class="widget-list-title"><a href="#">Overwatch Closed Beta</a></h4>
									<p><i class="fa fa-clock-o"></i> September 15, 2015</p>
								</div>
							</li>
							<li>
								<a href="#" class="thumb"><img src="img/blog/xs/2.jpg" alt=""></a>
								<div class="widget-list-meta">
									<h4 class="widget-list-title"><a href="#">Blood and Gore</a></h4>
									<p><i class="fa fa-clock-o"></i> September 13, 2015</p>
								</div>
							</li>
							<li>
								<a href="#" class="thumb"><img src="img/blog/xs/3.jpg" alt=""></a>
								<div class="widget-list-meta">
									<h4 class="widget-list-title"><a href="#">Warner Bros. Interactive</a></h4>
									<p><i class="fa fa-clock-o"></i> September 12, 2015</p>
								</div>
							</li>
							<li>
								<a href="#" class="thumb"><img src="img/blog/xs/4.jpg" alt=""></a>
								<div class="widget-list-meta">
									<h4 class="widget-list-title"><a href="#">Sharks Don't Sleep</a></h4>
									<p><i class="fa fa-clock-o"></i> September 10, 2015</p>
								</div>
							</li>
							<li>
								<a href="#" class="thumb"><img src="img/blog/xs/5.jpg" alt=""></a>
								<div class="widget-list-meta">
									<h4 class="widget-list-title"><a href="#">GTA 5 Reaches 5 Million</a></h4>
									<p><i class="fa fa-clock-o"></i> September 10, 2015</p>
								</div>
							</li>
						</ul>
					</div>-->
					
					
					<!--
					<div class="widget">
						<div class="title">Recent Tweets</div>
						<div id="twitter" data-twitter="346548853320851456"></div>
					</div>-->
					
					
					<!-- <div class="widget">
						<div class="title">Facebook</div>
						<div class="widget-row">
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "http://connect.facebook.net/en-GB/sdk.js#xfbml=1&version=v2.5";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-page" data-href="https://www.facebook.com/yakuzi.eu" data-height="800" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/yakuzi.eu"><a href="https://www.facebook.com/yakuzi.eu">yakuzi.eu</a></blockquote></div></div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</section>
</div>