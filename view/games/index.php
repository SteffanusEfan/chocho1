<div id="wrapper">
	<section class="hero hero-games height-300" style="background-image: url(/showfile/show?filename = <?php echo $datab['filename'] ?>);">
		<div class="hero-bg"></div>
		<div class="container">
			<div class="page-header">
				<!--
				<div class="page-title bold"><a href="games-single.html">The Witcher 3: Wild Hunt</a></div>
								<p>New battle mechanics, a fantastic storyline, and a gritty setting make The Witcher one of the most engrossing RPGs.</p>-->
				
			</div>
		</div>
	</section>
	
	<section class="padding-top-25 no-padding-bottom border-bottom-1 border-grey-300">
		<div class="container">
			<div class="headline">
				<h4>Games</h4>
				<!--
				<div class="btn-group pull-right">
									<a href="#" class="btn btn-default"><i class="fa fa-th-large no-margin"></i></a>
									<a href="#" class="btn btn-default"><i class="fa fa-bars no-margin"></i></a>
								</div>
								
								<input type="text" class="form-control hidden-xs" placeholder="Search Game...">
									
								<div class="dropdown">
									<a href="#" class="btn btn-default btn-icon-left btn-icon-right dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sort-amount-desc"></i> Sort by <i class="ion-android-arrow-dropdown"></i></a>
									<ul class="dropdown-menu">
										<li><a href="#">Date</a></li>
										<li><a href="#">Score</a></li>
										<li><a href="#">A-Z</a></li>
									</ul>
								</div>
							
								<div class="dropdown">
									<a href="#" class="btn btn-default btn-icon-left btn-icon-right dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gamepad"></i> Platform <i class="ion-android-arrow-dropdown"></i></a>
									<ul class="dropdown-menu">
										<li><a href="#">Playstation 4</a></li>
										<li><a href="#">Steam</a></li>
										<li><a href="#">Xbox One</a></li>
										<li><a href="#">PC</a></li>
									</ul>
								</div>
										
								<div class="dropdown">
									<a href="#" class="btn btn-default btn-icon-left btn-icon-right dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar-o"></i> Release Date <i class="ion-android-arrow-dropdown"></i></a>
									<ul class="dropdown-menu">
										<li><a href="#">Released</a></li>
										<li><a href="#">Unreleased</a></li>
										<li><a href="#">2016</a></li>
										<li><a href="#">2015</a></li>
										<li><a href="#">2014</a></li>
									</ul>
								</div>-->
				
			</div>
		</div>
	</section>
	
	<section class="bg-grey-50">
		<div class="container">
			<div class="row">
				<?php
					foreach ($datag as $vg) {
						echo '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card">
								<div class="card-img">
									<a href="games-single.html"><img src="/showfile/show?filename = '.$vg['foto'].'" alt=""></a>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>15</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="/games/detail?idgames = '.$vg['_id'].'">'.$vg['name'].'</a></h3>
									<ul><li>'.date('dd-mm-YY', $vg['time_created']).'</li></ul>
									<p>'.$vg['description'].'</p>
								</div>
							</div>
						</div>';
					}
				?>
			</div>
			
			<!--
			<ul class="pagination margin-top-20">
							<li class="disabled"><a href="#">Previous</a></li>
							<li class="disabled"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">7</a></li>
							<li><a href="#">8</a></li>
							<li><a href="#">...</a></li>
							<li><a href="#">124</a></li>
							<li><a href="#">125</a></li>
							<li><a href="#">Next</a></li>
						</ul>-->
			
		</div>
	</section>
</div>