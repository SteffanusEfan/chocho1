<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#141619">
	
	<title>Chocho1</title>
	
	<!-- FAVICON -->
	<link rel="shortcut icon" href="img/favicon.ico">
	
	<!-- CORE CSS -->
	<?php
		foreach($font as $datfont)
		{
			echo '<link rel="stylesheet" href="'.$datfont.'">';
		}
	?>
	
	<?php
		foreach($css as $datcss)
		{
			echo '<link rel="stylesheet" href="'.$datcss.'">';
		}
	?>
    
</head>

<body class="fixed-header">
	<!-- /header -->
	<?php echo $menuatas; ?>
	<!-- /header -->
	
	<!-- <div class="modal-search">
		<div class="container">
			<input type="text" class="form-control" placeholder="Type to search...">
			<i class="fa fa-times close"></i>
		</div>
	</div>-->
	
	<!-- /.modal-search -->
	
	<?php echo $content; ?>
	<!-- wrapper --> 
	<!--
	<div id="wrapper">	
			<div id="full-carousel" class="ken-burns carousel slide full-carousel carousel-fade" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#full-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#full-carousel" data-slide-to="1"></li>
					<li data-target="#full-carousel" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active inactiveUntilOnLoad">
						<img src="img/slideshow/1.jpg" alt="">
						<div class="container">
							<div class="carousel-caption">
								<h1 data-animation="animated animate1 bounceInDown">The Witcher 3: Wild Hunt</h1>
								<p data-animation="animated animate7 fadeInUp">The world is in chaos. The air is thick with tension and the smoke of burnt villages.</p>
								<a href="#signin" data-toggle="modal" class="btn btn-primary btn-lg btn-rounded" data-animation="animated animate10 fadeInDown">Became a member now</a>
							</div>		
						</div>
					</div>
					
					<div class="item">
						<img src="img/slideshow/2.jpg" alt="">
						<div class="container">
							<div class="carousel-caption">
								<h1 data-animation="animated animate1 fadeInDown">The Last of Us: Remastered</h1>
								<p data-animation="animated animate7 fadeIn">Survive an apocalypse on Earth in The Last of Us, a PlayStation 4-exclusive title by Naughty Dog.</p>
								<a href="#signin" data-toggle="modal" class="btn btn-primary btn-lg btn-rounded" data-animation="animated animate10 fadeIn">Became a member now</a>
							</div>
						</div>
					</div>
					
					<div class="item">
						<img src="img/slideshow/3.jpg" alt="">
						<div class="container">
							<div class="carousel-caption">
								<h1 data-animation="animated animate1 fadeIn">Star Wars: Battlefront 3</h1>
								<p data-animation="animated animate7 fadeIn">Galactic forces clash in this reboot of Star Wars Battlefront, the blockbuster shooter.</p>
								<a href="#signin" data-toggle="modal" class="btn btn-primary btn-lg btn-rounded" data-animation="animated animate10 fadeIn">Became a member now</a>
							</div>
						</div>
					</div>
								
					<a class="left carousel-control" href="#full-carousel" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
					<a class="right carousel-control" href="#full-carousel" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</a>
				</div>
			</div>
			
			<section class="bg-grey-50 border-bottom-1 border-grey-200 relative">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="title outline">
								<h4><i class="fa fa-heart"></i> Recent Games</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu leo lobortis, aliquam dui.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/1.jpg" alt="">
									<div class="category"><span class="label label-warning">PC</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>15</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">Assassin's Creed Syndicate</a></h3>
									<ul>
										<li>March 11, 2016</li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit cras felis mauris, accumsan.</p>
								</div>
							</div>
						</div>
								
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/2.jpg" alt="">
									<div class="category"><span class="label label-success">Xbox One</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>8</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">Guardians of the Galaxy 2</a></h3>
									<ul>
										<li>March 15, 2016</li>
									</ul>
									<p>Donec maximus sodales tellus a molestie. In eu orci vitae ligula iaculis sollicitudin.</p>
								</div>
							</div>
						</div>
								
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/3.jpg" alt="">
									<div class="category"><span class="label label-primary">PS4</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>12</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">The Witcher 3: Wild Hunt</a></h3>
									<ul>
										<li>Febr 21, 2016</li>
									</ul>
									<p>Proin at efficitur purus. Suspendisse eu erat ante. Ut lectus arcu, mollis id eleifend et.</p>
								</div>
							</div>
						</div>	
						
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/4.jpg" alt="">
									<div class="category"><span class="label label-danger">Steam</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>10</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">Grand Theft Auto 5</a></h3>
									<ul>
										<li>Apr 21, 2016</li>
									</ul>
									<p>Suspendisse potenti. Ut pretium, erat a cursus bibendum, nisi lectus egestas.</p>
								</div>
							</div>
						</div>
								
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/5.jpg" alt="">
									<div class="category"><span class="label label-primary">PS4</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>4</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">Deadpool The Game</a></h3>
									<ul>
										<li>Unknown Release Date</li>
									</ul>
									<p>Nullam eu tellus feugiat, lobortis ante a, pulvinar magna.</p>
								</div>
							</div>
						</div>
								
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="card card-hover">
								<div class="card-img">
									<img src="img/blog/md/6.jpg" alt="">
									<div class="category"><span class="label label-success">Xbox One</span></div>
									<div class="meta"><a href="games-single.html"><i class="fa fa-heart-o"></i> <span>16</span></a></div>
								</div>
								<div class="caption">
									<h3 class="card-title"><a href="games-single.html">Grand Theft Auto One</a></h3>
									<ul>
										<li>May 30, 2016</li>
									</ul>
									<p> Pellentesque id justo molestie, sodales leo nec, molestie nulla.</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="text-center"><a href="games.html" class="btn btn-primary btn-lg btn-shadow btn-rounded btn-icon-right margin-top-10 margin-bottom-20">Show More <i class="fa fa-angle-right"></i></a></div>
				</div>
			</section>
					
			<section>	
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="title outline">
								<h4><i class="fa fa-star"></i> Recent Reviews</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu leo lobortis, aliquam dui.</p>
							</div>
						</div>
					</div>
					<div class="row slider">
						<div class="owl-carousel">
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/1.jpg" alt="">
									<span class="label label-success">7.2</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Uncharted 4</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/2.jpg" alt="">
									<span class="label label-warning">5.4</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Hitman: Absolution</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/3.jpg" alt="">
									<span class="label label-danger">2.1</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Last of Us 2</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/4.jpg" alt="">
									<span class="label label-success">6.9</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Bioshock: Infinite</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/5.jpg" alt="">
									<span class="label label-success">7.2</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Grand Theft Auto 5</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/6.jpg" alt="">
									<span class="label label-warning">5.4</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">DayZ</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
							
							<div class="card card-list">
								<div class="card-img">
									<img src="img/review/7.jpg" alt="">
									<span class="label label-danger">2.1</span>
								</div>
								<div class="caption">
									<h4 class="card-title"><a href="reviews-single.html">Liberty City</a></h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>
							</div>
						</div>
						<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
						<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</section>
				
			<div class="background-image margin-top-40" style="background-image: url(http://img.youtube.com/vi/IsDX_LiJT7E/maxresdefault.jpg);">
				<span class="background-overlay"></span>
				<div class="container">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IsDX_LiJT7E?rel=0&amp;showinfo=0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
				
			<section class="bg-success subtitle-lg">
				<div class="container">
					<h2>Create your own epic gaming site with gameforest template</h2>
					<a href="http://themeforest.net/item/gameforest-responsive-gaming-html-theme/5007730" target="_blank" class="btn btn-white btn-outline">Purchase Now<i class="fa fa-shopping-cart margin-left-10"></i></a>
				</div>
			</section>
		</div>-->
	
	<!-- /#wrapper -->
	
	<!-- footer -->
	<footer>
		<div class="container">
			<div class="widget row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<h4 class="title">About Chocho1</h4>
					<?php
						$db = Db::init();
						$col = $db->preference;
						$datap = $col->findone();
						
						echo '<p>'.$datap['description'].'</p>';
					?>
					<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra mattis arcu, a congue leo malesuada eu. Nam nec mauris ut odio tristique varius et eu metus. Quisque massa purus, aliquet quis blandit et, <br /> <br />mollis sed lorem. Sed vel tincidunt elit. Phasellus at varius odio, sit amet fermentum mauris.</p>-->
				</div>
					
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<h4 class="title">Menu</h4>
					<div class="row">
						<!--
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
													<ul class="nav">
														<li><a href="#">Playstation 4</a></li>
														<li><a href="#">XBOX ONE</a></li>
														<li><a href="#">PC</a></li>
														<li><a href="#">PS3</a></li>
													</ul>
												</div>-->
						
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<ul class="nav">
								<li><a href="/">Home</a></li>
								<li><a href="/games/index">Games</a></li>
								<li><a href="/gallery/index">Gallery</a></li>
								<li><a href="/contactus/index">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
		
				<!--
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<h4 class="title">Email Newsletters</h4>
									<p>Subscribe to our newsletter and get notification when new games are available.</p>
									<form method="post" class="btn-inline form-inverse">
										<input type="text" class="form-control" placeholder="Email..." />
										<button type="submit" class="btn btn-link"><i class="fa fa-envelope"></i></button>
									</form>
								</div>-->
				
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">	
				<ul class="list-inline">
					<li><a href="#" class="btn btn-circle btn-social-icon" data-toggle="tooltip" title="Follow us on Twitter"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#" class="btn btn-circle btn-social-icon" data-toggle="tooltip" title="Follow us on Facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#" class="btn btn-circle btn-social-icon" data-toggle="tooltip" title="Follow us on Google"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#" class="btn btn-circle btn-social-icon" data-toggle="tooltip" title="Follow us on Steam"><i class="fa fa-steam"></i></a></li>
				</ul>
				&copy; 2016 Chocho1. All rights reserved.
			</div>
		</div>
	</footer>	
	<!-- /.footer -->
	
	<!--
	<div id="signin" class="modal fade" tabindex="-1">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title"><i class="fa fa-user"></i> Sign In to Gameforest</h3>
					</div>
					<div class="modal-body">
						<a class="btn btn-block btn-social btn-facebook"><i class="fa fa-facebook"></i> Connect with Facebook</a>
						<div class="separator"><span>or</span></div>								
						<form>
							<div class="form-group input-icon-left">
								<i class="fa fa-user"></i>
								<input type="text" class="form-control" name="username" placeholder="Username">
							</div>
							<div class="form-group input-icon-left">
								<i class="fa fa-lock"></i>
								<input type="password" class="form-control" name="password" placeholder="Password">
							</div>
							<button type="button" class="btn btn-primary btn-block">Sign In</button>
										
							<div class="form-actions">
								<div class="checkbox">
									<input type="checkbox" id="checkbox"> 
									<label for="checkbox">Remember me</label>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer text-left">
						Don't have Gameforest account? <a href="register.html">Sign Up Now</a>
					</div>
				</div>
			</div>
		</div>-->
	<!-- /.modal --> 
	
	<!-- Javascript -->
	<?php
		foreach($js as $datjs)
		{
			echo '<script src="'.$datjs.'"></script>';
		}
	?>
	<script>
	(function($) {
	"use strict";
		var owl = $(".owl-carousel");
			 
		owl.owlCarousel({
			items : 4, //4 items above 1000px browser width
			itemsDesktop : [1000,3], //3 items between 1000px and 0
			itemsTablet: [600,1], //1 items between 600 and 0
			itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
		});
			 
		$(".next").click(function(){
			owl.trigger('owl.next');
			return false;
		})
		$(".prev").click(function(){
			owl.trigger('owl.prev');
			return false;
		})
	})(jQuery);
	</script>
</body>
</html>