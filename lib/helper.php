<?php
class helper
{
	public static function rubahformattanggal($date) {
		$cari = array(
			'January','February','March','April','May','June','July','August','September','October','November','December'
		);
		$rubah = array(
			'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'
		);
		
		return str_replace($cari, $rubah, $date);
	}
	
	public static function showDialog()
	{		
		echo '<div id="deleteDialog">';               
		echo '<div class="mws-dialog-inner"><p></p></div>';	
		echo '</div>';
	}
	
	public static function getPreferences(){
		$db = Db::init();
		$pref = $db->preference;
		$data = $pref->findOne();
		return $data;
	}

	public static function findexts($filename) {
		$path_parts = pathinfo($filename);
		return $path_parts['extension'];
	}
	
	public static function timetosecond($time) {
		if(strpos($time, ':') !== false) {
			$s = explode(':', $time);
			$ss = 0;			
			if(isset($s[0])) {
				$ss += intval($s[0])*60;
			}
			if(isset($s[1])) {
				$ss += intval($s[1]);
			}
				
			return $ss;
		}
		
		return $time;
	}
	
	public static function limitString($string, $limit = 100) 
	{
	    // Return early if the string is already shorter than the limit
	    if(strlen($string) < $limit) {return $string;}
	
	    $regex = "/(.{1,$limit})\b/";
	    preg_match($regex, $string, $matches);
	    return $matches[1].'...';
	}
	
	public static function checkOutBooking() {
		$json = array('merchant_id' => '56b06a991295950cd98b456d');
		$curl = new Curl();
		$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
		$curl->post('http://wallet.deboxs.com/api/token/get', json_encode($json));
		
		$h = json_decode($curl->response, true);
		
		$hh = $h['result'];
		return $hh;
	}
	
	public function getRatingDisplay()
	{
		//display rating
		$displayrating = "none";
		//left rating
		$leftrating = "0px";
		if(isset($_SESSION['userid']))
		{
			$displayrating = "block";
			$leftrating = "150px";	
		}
		$var = array(
		"displayrating" => $displayrating,
		"leftrating" => $leftrating
		);
		return $var;
	}		
	
	private static function hashData($salt, $pwd) {
		$salt = md5($salt.$pwd);
				
		$options = [
			'cost' => 11,
			'salt' => $salt,
		];
		$pwd = password_hash($pwd, PASSWORD_BCRYPT, $options);
		return $pwd;
	}
	
	public static function createPassword($salt, $pass) {
		$password = '';
		if(strlen(trim($pass)) > 0) {
			$password = self::hashData($salt,$pass);
		}
		
		return $password;
	}
	
	public static function checkPassword($password, $data) {
		if(password_verify($password, $data))
			return true;
		
		return false;
	}
	
	public static function getTime($time) {
		
		$t = date('d.m.Y', time());
		$tt = date('d.m.Y', $time);
		$his = date('H:i:s', $time);
		$s = 'Today | '.$his;
		if($t != $tt) {
			$s = $tt.' | '.$his;
		}
		
		return $s;
	}
	
	public static function getUserName($id) {
		$db = Db::init();
		$g = $db->user_guests;
		$gg = $g->findone(array('_id' => new MongoId($id)));
		
		return $gg['name'];
	}
	
	public static function getRandomString() 
	{
		$crypt = new CkCrypt2() ;

	    $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
	    if ($success != true) {
	        printf("%s\n",$crypt->lastErrorText());
	        return "";
	    }

		$crypt->put_CryptAlgorithm("aes");
	    $crypt->put_CipherMode("cbc");
	    $crypt->put_KeyLength(256);
	    $crypt->put_PaddingScheme(0);
	    $crypt->put_EncodingMode("hex");

    	return $crypt->genRandomBytesENC(32);
	}
	
	public static function getRatingMovie($movieid = null)
	{
		$rating = 0;	
		if(strlen($movieid)>0)
		{
			$db = Db::init();
			$ratingmoviedb = $db->rating_movies;
			$ratingmoviedata = $ratingmoviedb->find(array("movie" => trim($movieid)));
			$countratingmovie = $ratingmoviedb->count(array("movie" => trim($movieid)));
			
			$totalmovierating = 0;
			foreach($ratingmoviedata as $rd)
			{
				$totalmovierating += $rd['point'];
			}
			if($countratingmovie>0)
				$rating = ($totalmovierating/$countratingmovie);
			
		}
		return $rating;
	}
	
	public static function decrypt($enc)
    {
    	$token = $_SESSION['token'];
	    $key = helper::getKey($token);
		$crypt = new CkCrypt2();
		$crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		
		$crypt->SetEncodedKey($key,'hex');
		
		$crypt->SetEncodedIV('000102030405060708090A0B0C0D0E0F','hex');
		$decStr = $crypt->decryptStringENC($enc);
		return $decStr;
    }
	
	public static function decryptpreferences($enc)
    {
    	$token = $_SESSION['token'];
	    $key = helper::getKeyPreference($token);
		$crypt = new CkCrypt2();
		$crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		
		$crypt->SetEncodedKey($key,'hex');
		
		$crypt->SetEncodedIV('000102030405060708090A0B0C0D0E0F','hex');
		$decStr = $crypt->decryptStringENC($enc);
		return $decStr;
    }
	
	public static function encrypt($enc)
    {
    	$token = $_SESSION['token'];
	    $key = helper::getKey($token);
		
		$crypt = new CkCrypt2();
		$crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		
		$crypt->SetEncodedKey($key,'hex');
		
		$crypt->SetEncodedIV('000102030405060708090A0B0C0D0E0F','hex');
		$decStr = $crypt->encryptStringENC($enc);
		return $decStr;
    }
	
	public static function encryptpreferences($enc)
    {
    	$token = $_SESSION['token'];
	    $key = helper::getKeyPreference($token);
		
		$crypt = new CkCrypt2();
		$crypt->UnlockComponent('WILLAWCrypt_KM8tJZPHMRLn');
		$crypt->put_CryptAlgorithm('aes');
		$crypt->put_CipherMode('cbc');
		$crypt->put_KeyLength(256);
		
		$crypt->SetEncodedKey($key,'hex');
		
		$crypt->SetEncodedIV('000102030405060708090A0B0C0D0E0F','hex');
		$decStr = $crypt->encryptStringENC($enc);
		return $decStr;
    }
	
	public static function getKey($token)
    {    	
		$appid = $_SESSION['appid'];
		$db = Db::init();
		$app = $db->apps;
		$psd = $app->findone(array('appid' => $appid));
		$privKeyXml=$psd['secretkey'];
		
		$rsa = new CkRsa();
		$rsa->UnlockComponent('WILLAWRSA_sYzUBmQejHnW');
		$rsa->ImportPrivateKey($privKeyXml);
		$rsa->put_EncodingMode('base64');
		
		$bUsePrivateKey = true;
		$key = $rsa->decryptStringENC($token,$bUsePrivateKey);
		return $key;
    }
	
	public static function getKeyPreference($token)
    {    	
		$appid = $_SESSION['appid'];
		$db = Db::init();
		$preference = $db->preferences;
		$psd = $preference->findone(array('appid' => $appid));
		$key = $psd['key'];
		return $key;
    }
	
}