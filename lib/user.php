<?php

class user
{	
	public function getRandomString() {
		$crypt = new CkCrypt2() ;

	    $success = $crypt->UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
	    if ($success != true) {
	        printf("%s\n",$crypt->lastErrorText());
	        return "";
	    }

		$crypt->put_CryptAlgorithm("aes");
	    $crypt->put_CipherMode("cbc");
	    $crypt->put_KeyLength(256);
	    $crypt->put_PaddingScheme(0);
	    $crypt->put_EncodingMode("hex");

    	return $crypt->genRandomBytesENC(32);
	}
	
	private function hashData($salt, $pwd) {
		$salt = md5($salt.$pwd);
				
		$options = [
			'cost' => 11,
			'salt' => $salt,
		];
		$pwd = password_hash($pwd, PASSWORD_BCRYPT, $options);
		return $pwd;
	}
	
	public function createPassword($salt, $pass) {
		$password = '';
		if(strlen(trim($pass)) > 0) {
			$password = $this->hashData($salt,$pass);
		}
		
		return $password;
	}
	
	public function checkPassword($password, $data) {
		if(password_verify($password, $data))
			return true;
		
		return false;
	}
}
